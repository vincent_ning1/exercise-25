﻿using System;

namespace exercise_25
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            var Vincent = new Person("Vincent",18);
            
            Vincent.hello("Vincent",18);

            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("----------b----------");
            
            var a1 = new Checkout(3, 4.50);
            double b1 = a1.Value();

            var a2 = new Checkout(2, 12.40);
            double b2 = a2.Value();

            var a3 = new Checkout(7, 1.43);
            double b3 = a3.Value();

            var a4 = new Checkout(8, 89.43);
            double b4 = a4.Value();

            var a5 = new Checkout(2, 76.32);
            double b5 = a5.Value();

            Console.WriteLine(b1+b2+b3+b4+b5);
            Console.WriteLine(a1.Value()+a2.Value()+a3.Value()+a4.Value()+a5.Value());
            

            
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();




        }

    
    }

    class Person
    {

        private string Name = "";
        private int Age;

        public Person(string _name,int _age)
        {
            Name = _name; 
            Age = _age;
        }
        
        public void hello(string _name,int _age)
            {
                Console.WriteLine($"My name is {_name}, I am {_age} years old");
            }  
           
    }
   class Checkout
    {
        int Quantity;
        double Price;

        public Checkout(int quantity,double price)
        {
            Quantity = quantity;
            Price = price;
        }
       public double Value()
        {
            return  Quantity*Price;
            
            
        }

    }
}
